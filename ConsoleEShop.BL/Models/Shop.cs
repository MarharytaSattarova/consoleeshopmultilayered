﻿using ConsoleEShop.DAL.Interfaces;
using ConsoleEShop.DAL.UnitOfWork;

namespace ConsoleEShop.BL.Models
{
    public class Shop
    {
        private static Shop instance;
        private static readonly object locker = new object();

        public IUnitOfWork UnitOfWork { get; set; } = new UnitOfWork();

        Shop() { }

        public static Shop Instance
        {
            get
            {
                lock (locker)
                {
                    return instance ?? (instance = new Shop());
                }
            }
        }
    }
}
