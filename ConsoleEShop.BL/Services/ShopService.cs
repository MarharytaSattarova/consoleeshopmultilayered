﻿using ConsoleEShop.BL.Interfaces;
using ConsoleEShop.BL.Models;
using ConsoleEShop.DAL.Entities;
using ConsoleEShop.DAL.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace ConsoleEShop.BL.Services
{
    public class ShopService : IShopService
    {
        private readonly Shop shopInstance;

        public ShopService()
        {
            shopInstance = Shop.Instance;
        }

        public User LogIn(string login, string password)
        {
            var currentUser = shopInstance.UnitOfWork.Users.Get().FirstOrDefault(x => x.Login == login && x.Password == password);

            if (currentUser == null) throw new InvalidOperationException("User with such login and password not found.");

            return currentUser;
        }

        public User Register(User newUserInfo)
        {
            if (string.IsNullOrWhiteSpace(newUserInfo.Login) || string.IsNullOrWhiteSpace(newUserInfo.Password)) throw new ArgumentException("Login/Password cannot be an empty string.");
            if (!IsValidName(newUserInfo.FirstName, newUserInfo.LastName)) throw new ArgumentException("First name/last name field has invalid value.");
            if (!IsValidEmail(newUserInfo.Email)) throw new ArgumentException("Invalid email.");

            newUserInfo.Id = shopInstance.UnitOfWork.Users.Get().Count;
            shopInstance.UnitOfWork.Users.Create(newUserInfo);

            return newUserInfo;
        }

        private bool IsValidEmail(string email)
        {
            var pattern = new Regex(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", RegexOptions.IgnoreCase);

            return pattern.IsMatch(email);
        }

        private bool IsValidName(string firstName, string lastName)
        {
            var pattern = new Regex(@"^([^\s]+)$", RegexOptions.IgnoreCase);

            if (pattern.IsMatch(firstName) && pattern.IsMatch(lastName))
                return true;

            return false;

        }

        public List<Product> GetProducts()
        {
            return shopInstance.UnitOfWork.Products.Get();
        }

        public Product GetProduct(string name)
        {
            var searchedProduct = shopInstance.UnitOfWork.Products.Get().FirstOrDefault(x => x.Name == name);

            if (searchedProduct == null) throw new InvalidOperationException("Product with such name not found.");

            return searchedProduct;
        }

        public Product GetProduct(int id)
        {
            var searchedProduct = shopInstance.UnitOfWork.Products.Get(id);

            if (searchedProduct == null) throw new InvalidOperationException("Product with such Id not found.");

            return searchedProduct;
        }

        public Order CreateOrder(Order newOrderInfo)
        {
            if (shopInstance.UnitOfWork.Users.Get().FirstOrDefault(x => x.Id == newOrderInfo.UserId) == null) throw new InvalidOperationException("User with Id specified in the field UserId does not exist.");

            newOrderInfo.Id = shopInstance.UnitOfWork.Orders.Get().Count;
            newOrderInfo.Status = OrderStatus.New;
            newOrderInfo.CreatedAt = DateTime.Now;

            shopInstance.UnitOfWork.Orders.Create(newOrderInfo);

            return newOrderInfo;
        }

        public void ProceedOrder(User currentUser, int orderId)
        {
            var order = shopInstance.UnitOfWork.Orders.Get().FirstOrDefault(x => x.Id == orderId && x.UserId == currentUser.Id);

            if (order == null) throw new ArgumentException("Order with such id not found.", "orderId");

            if (order.Status == OrderStatus.PaymentReceived || order.Status == OrderStatus.Sent) throw new InvalidOperationException("This order is already in progress.");

            if (order.Status == OrderStatus.CanceledByAdmin) throw new InvalidOperationException("This order has been canceled by administrator.");

            Pay(currentUser, order);
        }

        private void Pay(User user, Order order)
        {
            if (user.Balance < order.Total) throw new InvalidOperationException("Not enough money on the balance to pay for this order.");

            user.Balance -= order.Total;
        }

        public void CancelOrder(User currentUser, int orderId)
        {
            var order = shopInstance.UnitOfWork.Orders.Get().FirstOrDefault(x => x.Id == orderId && x.UserId == currentUser.Id);

            if (order == null) throw new InvalidOperationException("Order with such id not found.");

            if (order.Status == OrderStatus.Received) throw new InvalidOperationException("Unable to cancel order because order status is 'Received'.");

            if (order.Status == OrderStatus.CanceledByAdmin || order.Status == OrderStatus.CanceledByUser) throw new InvalidOperationException("Order is already canceled.");

            if (order.Status != OrderStatus.New)
            {
                currentUser.Balance += order.Total;
            }

            order.Status = OrderStatus.CanceledByUser;
        }

        public List<Order> GetUserOrders(int userId)
        {
            if (shopInstance.UnitOfWork.Users.Get().FirstOrDefault(x => x.Id == userId) == null) throw new ArgumentException("User with such Id does not exist.", "userId");

            return shopInstance.UnitOfWork.Orders.Get().Where(x => x.UserId == userId).ToList();
        }

        public void SetOrderStatus(int orderId, OrderStatus newStatus)
        {
            var searchedOrder = shopInstance.UnitOfWork.Orders.Get().FirstOrDefault(x => x.Id == orderId);

            if (searchedOrder == null) throw new ArgumentException("Order with such Id not found.", "orderId");            

            searchedOrder.Status = newStatus;

            if (newStatus == OrderStatus.Completed)
                searchedOrder.FinishedAt = DateTime.Now;
        }

        public void UpdateUserInfo(int userId, User newUserInfo)
        {
            if (!IsValidName(newUserInfo.FirstName, newUserInfo.LastName)) throw new InvalidOperationException("Name is invalid.");
            if (!IsValidEmail(newUserInfo.Email)) throw new InvalidOperationException("Email is invalid.");
            if (string.IsNullOrWhiteSpace(newUserInfo.Login) || string.IsNullOrWhiteSpace(newUserInfo.Password)) throw new InvalidOperationException("Login/password cannot be an empty string.");

            shopInstance.UnitOfWork.Users.Update(newUserInfo, userId);
        }

        public void TopUpUserBalance(decimal sum, User user)
        {
            if (sum <= 0) throw new InvalidOperationException("Sum to put on the balance has to be a non-zero value.");

            user.Balance += sum;
        }

        public User GetUserInfo(int userId)
        {
            return shopInstance.UnitOfWork.Users.Get(userId);
        }

        public List<User> GetUsers()
        {
            return shopInstance.UnitOfWork.Users.Get();
        }

        public Product CreateProduct(Product product)
        {
            if (string.IsNullOrWhiteSpace(product.Name)) throw new InvalidOperationException("Product name cannot be an empty string.");
            if (product.Price <= 0) throw new InvalidOperationException("Product price must be a non-zero value.");

            product.Id = shopInstance.UnitOfWork.Products.Get().Count;

            shopInstance.UnitOfWork.Products.Create(product);

            return product;
        }

        public void UpdateProductInfo(int productId, Product newProductInfo)
        {
            if (string.IsNullOrWhiteSpace(newProductInfo.Name) || string.IsNullOrWhiteSpace(newProductInfo.Description))
                throw new InvalidOperationException("Product name/description cannot be an empty string.");

            if (newProductInfo.Price <= 0) throw new InvalidOperationException("Product price must be a non-zero number.");

            shopInstance.UnitOfWork.Products.Update(newProductInfo, productId);
        }
    }
}
