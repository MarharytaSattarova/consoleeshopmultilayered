﻿using ConsoleEShop.DAL.Entities;
using ConsoleEShop.DAL.Enums;
using System.Collections.Generic;

namespace ConsoleEShop.BL.Interfaces
{
    public interface IShopService
    {
        public User LogIn(string login, string password);
        public User Register(User newUserInfo);
        public List<Product> GetProducts();
        public Product GetProduct(string name);
        public Order CreateOrder(Order newOrderInfo);
        public void ProceedOrder(User currentUser, int orderId);
        public void CancelOrder(User currentUser, int orderId);
        public List<Order> GetUserOrders(int userId);
        public void SetOrderStatus(int orderId, OrderStatus newStatus);
        public void UpdateUserInfo(int userId, User newUserInfo);
        public User GetUserInfo(int userId);
        public List<User> GetUsers();
        public Product CreateProduct(Product product);
        public void UpdateProductInfo(int productId, Product newProductInfo);

    }
}
