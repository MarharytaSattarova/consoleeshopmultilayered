﻿using ConsoleEShop.BL.Services;
using ConsoleEShop.Client.MenuProvider;
using System;

namespace ConsoleEShop
{
    class Program
    {
        static void Main(string[] args)
        {
            var shopService = new ShopService();

            while (MenuProvider.toContinue)
                MenuProvider.ProvideMenu(shopService);
        }
    }
}
