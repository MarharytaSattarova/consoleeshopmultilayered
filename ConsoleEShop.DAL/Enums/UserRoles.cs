﻿namespace ConsoleEShop.DAL.Enums
{
    public enum UserRoles
    {
        Guest,
        RegisteredUser,
        Admin
    }
}
