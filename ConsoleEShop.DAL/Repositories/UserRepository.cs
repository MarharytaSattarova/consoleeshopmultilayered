﻿using ConsoleEShop.DAL.Context;
using ConsoleEShop.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEShop.DAL.Repositories
{
    public class UserRepository : BaseRepository<User>
    {
        public UserRepository(ApplicationContext context) : base(context) { }

        public override void Delete(int id)
        {
            var itemToDelete = context.Users.FirstOrDefault(x => x.Id == id);

            if (itemToDelete == null) throw new InvalidOperationException("User with such Id does not exist.");

            context.Users.Remove(itemToDelete);
        }

        public override List<User> Get()
        {
            return context.Users;
        }

        public override User Get(int id)
        {
            var searchedUser = context.Users.FirstOrDefault(x => x.Id == id);

            if (searchedUser == null) throw new ArgumentException("User with such Id not found.", "id");

            return searchedUser;
        }

        public override void Update(User entity, int id)
        {
            var searchedUser = context.Users.FirstOrDefault(x => x.Id == id);

            if (searchedUser == null) throw new ArgumentException("User with such Id not found.", "id");

            searchedUser.Login = entity.Login;
            searchedUser.Password = entity.Password;
            searchedUser.FirstName = entity.FirstName;
            searchedUser.LastName = entity.LastName;
            searchedUser.Email = entity.Email;
        }
    }
}
