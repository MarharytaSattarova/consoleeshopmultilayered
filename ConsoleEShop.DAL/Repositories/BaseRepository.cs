﻿using ConsoleEShop.DAL.Context;
using ConsoleEShop.DAL.Entities;
using ConsoleEShop.DAL.Interfaces;
using System.Collections.Generic;

namespace ConsoleEShop.DAL.Repositories
{
    public abstract class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        protected readonly ApplicationContext context;

        protected BaseRepository(ApplicationContext context)
        {
            this.context = context;
        }

        public virtual void Create(TEntity entity)
        {
            switch (entity)
            {
                case User user: context.Users.Add(user);
                    break;
                case Order order: context.Orders.Add(order);
                    break;
                case Product product: context.Products.Add(product);
                    break;
            }
        }

        public virtual void Delete(TEntity entity)
        {
            switch (entity)
            {
                case User user:
                    context.Users.Remove(user);
                    break;
                case Order order:
                    context.Orders.Remove(order);
                    break;
                case Product product:
                    context.Products.Remove(product);
                    break;
            }
        }

        public abstract void Delete(int id);

        public abstract List<TEntity> Get();

        public abstract TEntity Get(int id);

        public abstract void Update(TEntity entity, int id);
    }
}
