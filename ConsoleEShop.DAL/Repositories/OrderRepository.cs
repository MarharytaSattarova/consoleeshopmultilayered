﻿using ConsoleEShop.DAL.Context;
using ConsoleEShop.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEShop.DAL.Repositories
{
    public class OrderRepository : BaseRepository<Order>
    {
        public OrderRepository(ApplicationContext context) : base(context) { }

        public override void Delete(int id)
        {
            var itemToDelete = context.Orders.FirstOrDefault(x => x.Id == id);

            if (itemToDelete == null) throw new InvalidOperationException("Order with such Id does not exist.");

            context.Orders.Remove(itemToDelete);
        }

        public override List<Order> Get()
        {
            return context.Orders;
        }

        public override Order Get(int id)
        {
            return context.Orders.FirstOrDefault(x => x.Id == id);
        }

        public override void Update(Order entity, int id)
        {
            var searchedOrder = context.Orders.FirstOrDefault(x => x.Id == id);

            if (searchedOrder == null) throw new InvalidOperationException("Order with such Id not found.");

            searchedOrder.Items = entity.Items;
            searchedOrder.Status = entity.Status;
            searchedOrder.Total = entity.Total;
            searchedOrder.UserId = entity.UserId;               
        }
    }
}
