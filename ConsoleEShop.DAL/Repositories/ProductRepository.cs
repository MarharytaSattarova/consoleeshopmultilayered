﻿using ConsoleEShop.DAL.Context;
using ConsoleEShop.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEShop.DAL.Repositories
{
    public class ProductRepository : BaseRepository<Product>
    {
        public ProductRepository(ApplicationContext context) : base(context) { }

        public override void Delete(int id)
        {
            var itemToDelete = context.Products.FirstOrDefault(x => x.Id == id);

            if (itemToDelete == null) throw new InvalidOperationException("Product with such Id does not exist.");

            context.Products.Remove(itemToDelete);
        }

        public override List<Product> Get()
        {
            return context.Products;
        }

        public override Product Get(int id)
        {
            return context.Products.FirstOrDefault(x => x.Id == id);
        }

        public override void Update(Product entity, int id)
        {
            var searchedProduct = context.Products.FirstOrDefault(x => x.Id == id);

            if (searchedProduct == null) throw new InvalidOperationException("Product with such Id not found.");

            searchedProduct.Name = entity.Name;
            searchedProduct.Description = entity.Description;
            searchedProduct.Price = entity.Price;
            searchedProduct.Category = entity.Category;
        }
    }
}
