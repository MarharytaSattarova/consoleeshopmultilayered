﻿using ConsoleEShop.DAL.Entities;
using System.Threading.Tasks;

namespace ConsoleEShop.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<User> Users { get; }
        IRepository<Order> Orders { get; }
        IRepository<Product> Products { get; }

        int SaveChanges();

        Task<int> SaveChangesAsync();
    }
}
