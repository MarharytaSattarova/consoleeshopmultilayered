﻿using ConsoleEShop.DAL.Entities;
using System.Collections.Generic;

namespace ConsoleEShop.DAL.Context
{
    public class ApplicationContext
    {
        public List<User> Users { get; set; } = DataSource.Users;
        public List<Order> Orders { get; set; } = DataSource.Orders;
        public List<Product> Products { get; set; } = DataSource.Products;
    }
}
