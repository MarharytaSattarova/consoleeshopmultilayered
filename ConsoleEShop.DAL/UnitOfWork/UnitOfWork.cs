﻿using ConsoleEShop.DAL.Context;
using ConsoleEShop.DAL.Entities;
using ConsoleEShop.DAL.Interfaces;
using ConsoleEShop.DAL.Repositories;
using System;
using System.Threading.Tasks;

namespace ConsoleEShop.DAL.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationContext _context;
        private OrderRepository _orderRepository;
        private ProductRepository _productRepository;
        private UserRepository _userRepository;

        public UnitOfWork()
        {
            _context = new ApplicationContext();
        }

        public IRepository<User> Users
        {
            get
            {
                if (_userRepository == null)
                    _userRepository = new UserRepository(_context);
                return _userRepository;
            }
        }


        public IRepository<Order> Orders
        {
            get
            {
                if (_orderRepository == null)
                    _orderRepository = new OrderRepository(_context);
                return _orderRepository;
            }
        }

        public IRepository<Product> Products
        {
            get
            {
                if (_productRepository == null)
                    _productRepository = new ProductRepository(_context);
                return _productRepository;
            }
        }

        public int SaveChanges()
        {
            throw new NotImplementedException();
        }

        public Task<int> SaveChangesAsync()
        {
            throw new NotImplementedException();
        }
    }
}
