﻿using ConsoleEShop.DAL.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop.DAL.Entities
{
    public class User : BaseEntity
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public UserRoles Role { get; set; }
        public DateTime RegisteredAt { get; set; }
        public decimal Balance { get; set; }
    }
}
