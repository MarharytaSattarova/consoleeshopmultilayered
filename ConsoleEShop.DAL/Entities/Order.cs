﻿using ConsoleEShop.DAL.Enums;
using System;
using System.Collections.Generic;

namespace ConsoleEShop.DAL.Entities
{
    public class Order : BaseEntity
    {
        public List<Product> Items { get; set; }
        public decimal Total { get; set; }
        public OrderStatus Status { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
        public int UserId { get; set; }
    }
}
