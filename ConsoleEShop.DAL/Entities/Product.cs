﻿using ConsoleEShop.DAL.Enums;

namespace ConsoleEShop.DAL.Entities
{
    public class Product : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public ProductCategories Category { get; set; }
    }
}
